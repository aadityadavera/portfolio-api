//packages
const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const Stock = require("../models/stock");
//variables to be used many times are saved in one place 
const price = 500;  //assumed current price of each stock to be used for calculating returns
const sell = 'sell';
const buy = 'buy';

router.route('/addTrade')
	.post(function(req, res) {
		//create a trade object using the request body
		var trade = {
			_id : mongoose.Types.ObjectId(),
			type : String(req.body.type),  
			quantity : Number(req.body.quantity),
			price : Number(req.body.price),
			date : String(req.body.date)
		};
		if(trade.type == sell){
			req.body.quantity = -req.body.quantity;
		}
		//find the stock using name and push the trade object created to the trades array and increase the number of trades and holding quantity
		Stock.findOneAndUpdate({name : req.body.name},{$push : { trades : trade}, $inc : {"holding.number_of_trades" : 1, "holding.quantity": req.body.quantity}},{new: true}, (err, doc) =>{ 
			//if the stock is present, in the portfolio already
			if(doc) {
				console.log(doc.trades);
				if(req.body.type == buy) {
				   var totalCost = 0;
				   var totalSharesBought = 0;
				   doc.trades.forEach(function(element) {
				   	if(element.type == buy) {
				   		totalCost += element.quantity*element.price;
				   	    totalSharesBought += Number(element.quantity);
				   	} 
				   });
				   //if the traded added is a buy then we update the average buying price of the stock
				   Stock.findOneAndUpdate({name : req.body.name},{$set : {"holding.average_buying_price" : totalCost/totalSharesBought}},{new: true}, (err, doc) => {
				   	if(doc) {
				   		console.log(doc.holding);
				   		res.status(201).json({success : true, data : doc});  		
				   	}
				   });
				} else {
					//quantity less than 0,i.e, case where user sells more than he owns, remove the last added traded and update quantity and number of trades
					if(doc.holding.quantity < 0) {
						Stock.findOneAndUpdate({name: req.body.name}, {$pull : {trades : trade}, $inc : {"holding.number_of_trades" : -1, "holding.quantity": -req.body.quantity}}, {new : true}, (err,doc) => {
							if(doc) {
								console.log(doc);
								res.status(400).json({success : false, data: {message: "Cannot sell more than holding quantity", holding : doc.holding}});
							}
						});
					} else {
						res.status(201).json({success : true, data : doc});
					}
				}
			} else {
				//case when stock is not present in portfolio

				//first trade cannot be a sell
				if(req.body.type == sell) {
					console.log("Cannot sell before buying");
					res.status(201).json({success: false, data: {message : "Cannot sell before buying"}});
				} else {
					//creating a new stock object to be saved in the collection
					stock = new Stock();
					stock.name = req.body.name;
				    stock.trades.push(trade);
				    stock.holding.quantity = req.body.quantity;
				    stock.holding.average_buying_price = req.body.price;
				    stock.holding.number_of_trades = 1;
				    stock.save()
				    .then(result => {})
				    .catch(err => {console.log(err); res.status(500).json({success: false, data : {error: err}})});
				    res.status(201).json({
				    	success : true,
				    	data : stock
				    });
				}
			}
		});
	});
router.route('/updateTrade')
	.post(function(req, res) {
		//creating a trade object from request body
		var trade = {
			_id : mongoose.Types.ObjectId(req.body.id),
			type : String(req.body.type),  
			quantity : Number(req.body.quantity),
			price : Number(req.body.price),
			date : String(req.body.date),
			updated_at : String(req.body.date)
		};
		if(trade.type == sell){
			req.body.quantity = -req.body.quantity;
		}
		Stock.findOne({name : req.body.name}, (err, doc) =>{ 
			if(doc) {
				var totalQuantity = 0;
				var totalCost = 0;
				var totalSharesBought = 0;
				var flag = -1;
				//iterating throught all the trades to update the holding(quantity,average buying price)
				doc.trades.forEach(element => {
					if(element._id == req.body.id) {
						totalQuantity += Number(req.body.quantity);
						if(req.body.type == buy) {
							totalCost += req.body.quantity*req.body.price;
							totalSharesBought += Number(req.body.quantity);
						}
						flag = 1;
						trade.date = element.date;
					} else {
						if(element.type == buy) {
						    totalCost += element.quantity*element.price;
							totalSharesBought += Number(element.quantity);
						    totalQuantity += Number(element.quantity);	
						}  else {
							totalQuantity -= Number(element.quantity);
						}
					}
				});
				//trade with the provided id is not present
				if(flag == -1) {
					res.status(400).json({success: false, data : {message : 'Trade not found'}});
				} else if(totalQuantity < 0) { 
					res.status(400).json({success: false, data : {message : 'Cannot sell more than holding quantity'}});
				} else {
				    //updating the trade
				    Stock.findOneAndUpdate({name : req.body.name, "trades._id" : mongoose.Types.ObjectId(req.body.id)},{$set : {"trades.$" : trade, "holding.quantity" : totalQuantity, "holding.average_buying_price": totalCost/totalSharesBought}} ,{new: true}, (err, doc) => {
				   	if(doc) {
				   		res.status(201).json({success: true, data : doc});  		
				   	} else {
				   		res.status(500).json({success: false, data : {message : 'Not found', error : err}});
				   	}
				   });
				}
		} else {
			//requested stock is not present in the portfolio
			res.status(400).json({success: false, data : {message: "Stock not found"}});
		} 
	});
	});

router.route('/removeTrade')
	.post(function(req, res) {
		//find the requested stock
		Stock.findOne({name : req.body.name}, (err, doc) =>{ 
			if(doc) {
				console.log(doc.trades);
				var totalQuantity = 0;
				var totalCost = 0;
				var totalSharesBought = 0;
				var flag = -1;
				//iterate over the trades to update the holding for requested stock
				doc.trades.forEach(element => {
					if(element._id != req.body.id) {
						if(element.type == buy) {
						    totalCost += element.quantity*element.price;
							totalSharesBought += Number(element.quantity);
						    totalQuantity += Number(element.quantity);	
						}  else {
							totalQuantity -= Number(element.quantity);
						}
					} else {
						flag = 1;
					}
				});
				if(flag == -1) {
					res.status(400).json({success: false, data : {message : 'Trade not found'}});
				} else if(totalQuantity < 0) { 
					res.status(400).json({success: false, data : {message : 'Cannot remove this trade as holding quantity becomes negative'}});
				} else {
					Stock.findOneAndUpdate({name : req.body.name},{$pull : {trades : {_id : req.body.id}},$set : {"holding.quantity" : totalQuantity, "holding.average_buying_price": totalCost/totalSharesBought}, $inc : {"holding.number_of_trades" : -1}},
						{new: true}, (err, doc) => {
				   	if(doc) {
				   		console.log(doc);
				   		res.status(201).json({success : true, data : {message : 'Removed trade', stock : doc}});  		
				   	} else {
				   		res.status(500).json({success : false, data : {message : 'Not found', error : err}});
				   	}
				   });
				}
			} else {
				//requested stock is not present in the portfolio
				res.status(201).json({success: false, data : {message: "Stock not found"}});
			} 
		});
	});

//get the portfolio		
router.route('/')
    .get(function(req, res) {
		Stock.find(function(err, docs) {
			if (err)
				res.status(500).json({error: err});
			res.status(201).json({success: true, data : docs});
		});
});

//get holdings
router.route('/holdings')
    .get(function(req, res) {
		var holdings = [];
		Stock.find(function(err, docs) {
			if(err) 
				res.status(500).json({success: false, data : {error: err}});
			if(docs){
				docs.forEach(doc => {
					holdings.push({name : doc.name, quantity : doc.holding.quantity, average_buying_price : doc.holding.average_buying_price});
				});
				res.status(201).json({success: true, data: holdings});
			}
		});
	});

//get returns
router.route('/returns')
	.get(function(req, res) {
		var returns = [];
		Stock.find(function(err, docs) {
			if(err) 
				res.status(500).json({error: err});
			if(docs){
				docs.forEach(doc => {
					returns.push({name : doc.name, return : doc.holding.quantity*(price - doc.holding.average_buying_price),
						quantity : doc.holding.quantity, average_buying_price : doc.holding.average_buying_price, current_price : price});
				});
				res.status(201).json({success: true, data: returns});
			}
		});
	});
module.exports = router;
