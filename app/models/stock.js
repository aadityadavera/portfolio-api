const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var stockSchema = new Schema({
	name: String,
    trades : {type : Array, default : []},
	holding : {
		quantity : Number,
		average_buying_price : Number,
		number_of_trades : Number
	}
});

module.exports = mongoose.model('Stock', stockSchema);