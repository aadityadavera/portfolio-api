// call the packages we need
const express    = require('express');
const bodyParser = require('body-parser');
const app        = express();
const morgan     = require('morgan');
const Stock = require('./app/models/stock');
const tradeRoute = require('./app/routes/tradeRoute');
const mongoose   = require('mongoose');


// configure app
app.use(morgan('dev')); // log requests to the console

// configure body parser
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// DATABASE SETUP
mongoose.connect('mongodb://aditya:aditya1611@ds061370.mlab.com:61370/portfolio-api',{ useMongoClient: true }); // connect to our database

mongoose.Promise = require('bluebird');
// Handle the connection event
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));

db.once('open', function() {
  console.log("DB connection alive");
});

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );
  if (req.method === "OPTIONS") {
    res.header("Access-Control-Allow-Methods", "PUT, POST, PATCH, DELETE, GET");
    return res.status(200).json({});
  }
  next();
});
// All routes to be handled by tradeRoute.js
app.use("/portfolio", tradeRoute);
module.exports = app;